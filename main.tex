% Font size
\documentclass[12pt,a4paper,british]{article}

\usepackage{ifxetex}

\ifxetex
  \usepackage{fontspec}
\else
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{babel}
  \usepackage{lmodern}
  \usepackage{textcomp}
\fi


\usepackage[margin=1in]{geometry}

\usepackage{hyperref}

% license
\usepackage[
    type={CC},
    modifier={by-nc-sa},
    version={4.0},
]{doclicense}

\usepackage{epigraph}

% length of epigraph
\setlength\epigraphwidth{12cm}
% remove epigraph bar
\setlength\epigraphrule{0pt}

\renewcommand{\epigraphsize}{\large}

% This is broken for some reason
% \usepackage{csquotes}

% \MakeAutoQuote{"}

% Referencing
\usepackage[backend=biber,bibstyle=apa,citestyle=authoryear]{biblatex}

% My references for this essay are polluted with a note field: disable it
\AtEveryBibitem{\clearfield{note}}
\AtEveryBibitem{\clearfield{abstract}}
\AtEveryBibitem{\clearfield{keyword}}


\bibliography{references.bib}

% break bilbatex reference URL on number
\setcounter{biburlnumpenalty}{9000}

% packages for Header and Footer
\usepackage{lastpage}
\usepackage{fancyhdr} 

%!TeX spellcheck = en-GB


%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\title{\textbf{Evaluate the rights and freedoms of China's muslims.}}

\author{\textit{2270405}}

 % Date, use \date{} for no date
\date{\today}

% save these variables for later use under different name
\makeatletter
\let\doctitle\@title
\let\docauthor\@author
\let\docdate\@date
\makeatother

%----------------------------------------------------------------------------------------
%	HEADER AND FOOTER SECTION
%----------------------------------------------------------------------------------------
\pagestyle{fancy}

% sets both header and footer to nothing
\fancyhf{}

% Header
% remove horizontal header bar
\renewcommand{\headrulewidth}{0pt}
% left hand side header
\lhead{Chinese Politics - Essay}
% right hand side header
\rhead{\docauthor}
\setlength{\headheight}{15pt}

% Footer
% center of footer
\cfoot{\thepage\ of \pageref{LastPage}}

%------------------------------------------------------------------------

\begin{document}



% Print the title section
\maketitle

\vspace*{\fill}

\epigraph{"The dangerous clashes of the future are likely to arise from the interaction of Western arrogance, Islamic intolerance, and Sinic asser­tiveness."}{\fullcite[183]{huntingtonClashCivilizationsRemaking1996}}


\vspace*{\fill}

\pagebreak

% Introduction
\section*{}

In the People's Republic of China ("China" or PRC), it is Sinic intolerance that confronts Islamic assertiveness. As this essay will demonstrate, Muslims in China are relatively free, to the extent that they practice a "Sinicized" Islam. We will compare the freedoms of the \textit{Hui} and \textit{Uyghur} Muslim \textit{Minzu} which make up the vast majority of Chinese Muslims. We will evaluate the constitutional, legal and informal rights of Muslims in the PRC, notably through the lens of Chinese Ethnic Policy and \textit{Minjian} as well as the Chinese Communist Party's (CCP) supervising of Chinese Muslims' interaction with the Global \textit{Ummah} and lastly focusing on the policies of internment practised in Xinjiang.

% Main body
\section*{}

\subsection*{The Islams of China}

There are 21 Million Muslims in the PRC, 10.6 Million of whom are identified as \textit{Hui}, they are mainly Sunni with some adhering to the Salafi branch of Islam. The Hui, are more disparately settled in China and "better integrated" with Han Chinese society than the following largest Muslim Chinese \textit{Minzu}: the Uyghur \autocite[12-13]{stewartChineseMuslimsGlobal2017}.

The 10 Million Chinese Uyghur are concentrated in Xinjiang, the \textit{Xinjiang Uygur Autonomous Region} (XUAR) which became part of China relatively late. Thus the Uyghurs' "Central Asian" cultural roots and Turkic language estranges them more than the Hui from the rest of the predominantly Han Chinese population \autocites[7-10]{erieChinaIslamProphet2016}[339]{starrXinjiangChinaMuslim2004}. Contemporary Chinese scholars of Islam "belong to a larger Chinese Islamic culture [...] whereas the Uyghur Muslims of Xinjiang have their own academic culture and influential figures" \autocite[201]{lipmanIslamicThoughtChina2017}.

\textcite[531-532]{kuoRevisitingSalafijihadistThreat2012} explains that Uyghurs are mostly Sunni and practice Hanafi, a "relaxed" interpretation of Islamic Law compared to the Salafi interpretation. It should also be noted that the Uyghur practice of Islam has strong Sufi influences, a mysticism whose practices are deemed "unorthodox" by Salafi Islam. Lastly, the Uyghur's pre-Islamic history has shaped unique rituals within Uyghur interpretation of Islam.

\paragraph{}
Looking beyond the Uyghur interpretation, there are two major "poles" of thought in Chinese Islam as practiced by most other Muslim Minzu. On the one hand the posterity of Abd al-Wahhab preach a Salafi conception of Islam inspired from Saudi Arabian Wahhabism. Salafi Islam is closely related to "Islamic culture" and less accepting of the idea of a "culturally Chinese Islam", with scholar such as Ma Wanfu preaching an "Arabicized Islam" as well as other branches of Islam \autocites[13]{cherif-chebbiAbdAlWahhabLiu2016}[531,535]{kuoRevisitingSalafijihadistThreat2012}.

On the other hand, most Hui practice a "Neo-confucian" Islam \autocite{cherif-chebbiAbdAlWahhabLiu2016}. The descendants of Liu Zhi interpret the scriptures through Confucianism, thus, they have built a rapport of cultural understanding with the Han population. Such an interpretation of Islam has been deemed unorthodox by more "Arabo-centric" conception of islam, notably the descendants of Abd al-Wahhab with an \autocites{kuoRevisitingSalafijihadistThreat2012}{cherif-chebbiAbdAlWahhabLiu2016}.

\subsection*{Ethnic Policy}

Post World War II, the CCP pursued the Nationalist's attempts at classifying the Chinese population into different ethnic groups, further subdividing the Nationalist's thirteen \textit{Minzu} into fifty-six \autocite{leiboldEthnicPolicyChina2013}. Some have argued this "divide and rule" policy aimed at weakening ties between the newly created "ethnicities" \autocites{bovingdonHeteronomyItsDiscontents2004,leiboldEthnicPolicyChina2013}[377]{starrXinjiangChinaMuslim2004}. However it also adopted affirmative action measures for "the better-developed groups to help the under-developed ones by furnishing economic and cultural aid" \autocite[219]{xiaotongPluralityUnityConfiguration1988}.

More recently, a "Second Generation of Ethnic Policy Scholars" \autocite[68]{tobinWorryingEthnicityNew2015} have argued that politicization and institutionalization allows Ethnic groups to claim rights as a distinct \textit{Minzu}. Instead, this new wave of Ethnic Policy in China advocates for "cultural pluralism – political unity" \autocite[216]{maNewPerspectiveGuiding2007} as a remedy for ethnic conflict stemming from the affirmative action and consequent "tribal" separatism of early Chinese ethnic policies.

\subsection*{Ethnicization of Islam}

% TODO make a proper sentence here
The CCP's separation of Islam into \textit{Minzu}, which designates "both the collective unity of the Chinese nation/race and to its fifty-six ethnic communities" \autocite[14]{leiboldEthnicPolicyChina2013}, has resulted in an "Ethnicization" of Islam \autocite{cieciuraEthnicityReligionRepublicanEra2016}. In a sense, being Muslim in China is, by definition belonging to an ethnic group, so-called \textit{Minzu} \autocite[11]{brownDefiningRightPath2019}. Conversion to Islam often implies changing \textit{Minzu}, although "ethnic change is unidirectional: Han can become Hui, but Hui cannot become Han", a fact internalized by many of the Hui interviewed in \textcite[245]{gladneyMuslimChineseEthnic1996} underlining the \textit{Minzu} "hierarchy" \autocite[332]{starrXinjiangChinaMuslim2004}.

China has constructed Ethnic divisions precisely by dividing its people into Ethnic groups \autocite{brownDefiningRightPath2019} and thus "[shields] the domestic securitization of Chinese Islam from international Islam" \autocite[8]{mesbahiIslamSecurityNarratives2013} allowing it to simultaneously develop economic and diplomatic channels with majority Muslim states in Eurasia. In fact, "the state not only discourages connections between muslim groups within China, but actively discourages transnational identification" \autocite[6]{brownDefiningRightPath2019}.

\subsection*{Supervised Global \textit{Ummah}}

This is embodied in the CCP's export abroad of its policy of control over the Uyghur population, notably after the collapse of the Soviet Union as well as following 9/11. Some have noted the instrumentalization of the US "War on Terror" to crack down on Uyghurs and the Uyghur diaspora, notably by conflating Uyghur independentist groups and the Uyghur diaspora with "Islamic terrorism", notably Salafi radicalism, in order to avoid legitimizing it through official dialogue \autocites[131]{shichorDialogueDeafRole2018}[530]{kuoRevisitingSalafijihadistThreat2012}[183]{bachmanMakingXinjiangSafe2004}.  The Chinese State has pressured countries hosting the Uyghur diaspora (specifically, members of the "Shangai Five") to conduct "counter-terrorism" operations and extradite radical separatist groups, like the Turkestan Islamic Party (TIP) but also on non-radical independence movements such as the East Turkestan Independence Movement (ETIM) \autocites{humanrightswatchEradicatingIdeologicalViruses2018}{amnestyinternationalPeopleRepublicChina2004}[126]{bovingdonHeteronomyItsDiscontents2004}.

At time headed by Uyghurs, the Muslim World League's active involvement with the Uyghur diaspora and Chinese \textit{Hajji} (pilgrims to the Holy Sites of Islam), has contributed ideologically and financially to the development of \textit{Salafiyya} (Salafi) Islam throughout China \autocites[19]{balciCentralAsianRefugees2007}[152]{erieChinaIslamProphet2016}. Perhaps a symbol of the CCP's association between Islam and the Uyghur independentist movement is its close supervision of Hajj :  "Hui from Ningxia are four [...] times as likely to be on a hajj delegations as Uyghurs from Xinjiang" \autocite[62]{friedrichsSinoMuslimRelationsHan2017}. CCP control over the Hajj is an aspect of the State "sinicization" of Islam: prominent Imams have strong ties to Salafi Saudi religious institutions, in part due to Saudi control of holy sites, destination of the \textit{Hajj} and key for "exporting" Salafi ideology \autocites{stewartChineseMuslimsGlobal2017}[529,533]{kuoRevisitingSalafijihadistThreat2012}. The CCP has expressed concern over this development of a Salafi practice of Islam strongly networked to the Muslims abroad, thus it exerts control over "ideological" pilgrims by regulating exits of the Chinese territory \autocites[140]{bovingdonHeteronomyItsDiscontents2004}[40]{stewartChineseMuslimsGlobal2017}[61]{friedrichsSinoMuslimRelationsHan2017}\autocite{BackflowPreventionDispatching2017}. This vigilance results in part from article 36 of the Chinese constitution which, amongst other statement about religious practice in the PRC, states that "Religious bodies and religious affairs are not subject to any foreign domination" \autocite[481]{zhuProsecutingEvilCults2010}.

\subsection*{Religious freedom}

The PRC's "constitutional religious clauses are comparable to international standards" \autocite[485]{zhuProsecutingEvilCults2010} contrarily to the regulations and legislations on religious freedoms resulting from Constitutional interpretations. Chinese law scholars have distinguished between religious \textit{belief} protected by the constitution, whilst religious \textit{activities} are a social act, regulated by law \autocite[481]{zhuProsecutingEvilCults2010}. The fact that many religious beliefs are expressed through a cult renders this distinction harmful to the constitutional right to freedom of religious belief, especially since the Chinese executive is granted unchecked power for enforcing this legislation \autocite{zhuProsecutingEvilCults2010}. Furthermore, the interpretations of the constitutional clauses and subsequent legislation on religion have been accused of carrying an "instrumentalist approach of law-making tends to restrict the exercise of religious freedom to serve a political agenda" \autocite[501]{zhuProsecutingEvilCults2010}.

Firstly, the PRC's constitution and subsequent legislation (such as the so called "Document 19") assert the State's separation from any religious institution \autocites{bovingdonHeteronomyItsDiscontents2004}{zhuProsecutingEvilCults2010}. Paradoxically, the PRC officially recognizes only five religions, underlining the preference for legislative control over constitutional freedom of religion \autocites[320]{potterBeliefControlRegulation2003}[483]{zhuProsecutingEvilCults2010}. State influence has shaped Muslim institutional life through the Islamic Association of China, which follows the party doctrine of religious adaptation to Chinese society the so called "Sinicization" of Islam \autocites{brownDefiningRightPath2019}[343]{erieChinaIslamProphet2016}[61]{friedrichsSinoMuslimRelationsHan2017}.

Secondly, as noted by the same article 36 of the PRC constitution "The State protects normal religious activities. No one may make use of religion to engage in activities that disrupt public order, impair the health of citizens or interfere with the educational system of the state" \autocite[481]{zhuProsecutingEvilCults2010}. "Normal" religious activities have been interpreted as ones consisting in "lawful" clerics preaching "lawful" religious content in "lawful" places. In other words, mosques, preachers and their discourse are "lawful" in their registration with (and sometimes appointment by) State institutions such as the Islamic Association of China and the Bureau of Religious Affairs \autocites[485]{zhuProsecutingEvilCults2010}[61,63]{ercilasunUyghurCommunityDiaspora2018}[ch. 4]{erieChinaIslamProphet2016}.

\subsection*{Economic development and inequality}

Article 4 of the Chinese constitution outlines the right for an aligned economic development between Chinese ethnic minorities. Consequently, the Regional Ethnic Autonomy Law states the right to "self-government" for ethnic minorities in the PRC as well as the jurisdiction of this "self-government" over economic development policies and an adaptation of the Central Government's policy to "local circumstances" \autocites[81]{broxFringesHarmoniousSociety2014}[49]{hagelChinaRegionalEthnic2005}.

In Xinjiang, the \textit{Open Up the West} policy has brought considerable investment to the region whose Gross Domestic Product (GDP) growth stood at 8.1\% in 2010 (PRC GDP: 8.7\%). However, there exists a considerable growing North-South and urban-rural economic inequality in Xinjiang that has been demonstrated to be particularly unfavourable to Uyghurs who mainly reside in South Xinjiang and in rural areas \autocites{broxFringesHarmoniousSociety2014}{chaudhuriSurveyEconomicSituation2005}{bachmanMakingXinjiangSafe2004}.

On the other hand, the Ningxia province (with the highest proportion of Hui population) paints a different picture of Minzu inequality: \textcite{gustafssonWhyThereNo2014} and \textcite{shiEmpiricalAnalysisIncome2013} demonstrate that there is no significant income gap in the province between Hui and Han in both urban and rural areas.

\subsection*{Cultural rights and Identity}

Still in the Ningxia province, the Hui recouncil Islamic Law through \textit{Minjian}, a "collaboration" between the Chinese State and the people for reconciling informal norms and institutions with the Chinese Law within which they are not necessarily inscribed \autocite{erieChinaIslamProphet2016}. This is a form of accomodation of both State and Islamic law in Hui communities, a "localized" interpretation of Islamic Law \autocite[344]{erieChinaIslamProphet2016}. Except in criminal law, Minjiang is a prominent concept that delegates local enforcement of legislation to Hui communities. This adaptive Hui interpretation of Islamic law, notably reconciling private property and marriage with the CCP's ideology through \textit{Minjian}. This compromise allows for the Hui to integrate in Chinese economic life and the State bureaucracy \autocite{erieChinaIslamProphet2016} in a form of Islam with Chinese characteristics.

\paragraph{}
Regional autonomy legislation guarantees the protection of Minzu culture, notably through regional government's power to adapt educational material and aid for Minzu populations to attend "ethnic schools" aimed at a particular Minzu \autocites[ch. 7]{broxFringesHarmoniousSociety2014}[ch. 4]{erieChinaIslamProphet2016}[49]{hagelChinaRegionalEthnic2005}. However, control of religious discourse has notably spilled over into education. Such "ethnic schools" in Xinjiang have largely favoured Standard Mandarin over Uyghur to favour the Uyghurs' "entry in the job market" \autocites[ch. 7]{broxFringesHarmoniousSociety2014}[202-203]{erieChinaIslamProphet2016}.

In Linxia (Ningxia prefecture), the Hui have maintained Arabic in "ethnic schools", there has however been a State backend shift away from Islam, emphasizing the teaching of Arabic for business and science, aimed for example at the development of the West-bound Silk Road, to push Islam into the private sphere \autocites[206-214]{erieChinaIslamProphet2016}[496]{turnbullPursuitIslamicAuthenticity2015}.

\subsection*{Uyghur Internment}

Perhaps the most extreme CCP conception of Minzu "education" are the Xinjiang so-called "reeducation camps" as part of a "de-extremification" campaign with a broad definition of "Islamic Terrorism". The sum of 78 discovered government contracts for the construction of these camps stands at 755 Million RMB (107,092,197 US dollars at the time of writing) \autocite{zenzThoroughlyReformingThem2019}. Crossing these estimates with satelite imagery, notably from \textcite{zhangListReeducationCamps2019} gives an estimated 1 Million detaineesin Xinjiang, about 11.5\% of adult Uyghur and Kazakh population in the province.

So-called "Xinjiang Cables" (CPP internal document leaks) reveal that the CPG is particularly insistent on preventing any escapes, epidemics and "abnormal deaths" as well as ensuring extensive video surveillance and preventing information "leak" by preventing "student" communications \autocite{autonomousregionpartypoliticalandlegalaffairscommissionOpinionsFurtherStrengthening}. The CCP's "educational" agenda seeks to educate "students" in Mandarin, "Manner", "Family" and Law while resolving "ideological contradictions" all while operating in "Strict secrecy" \autocite{autonomousregionpartypoliticalandlegalaffairscommissionOpinionsFurtherStrengthening}

\section*{Concluding Thoughts}

Comparing the Hui and Uyghur experiences as Muslims in the PRC reveals widely differing rights and freedoms.

"Neo-confucian" Islam as practised by many Hui has created a culturally and theologically very unique space in Chinese society. Cohabitation of State and Islamic Law in the Linxia province is an impressive feat that challenges many "Western" multicultural integration experiences. Equally interesting is the shaping of Islam to fit in this "foreign" environment thus creating a "Neo-confucian" branch of Islam shaping local policies as much as it is shaped by them. Interestingly, the relatively successful Hui integration is perhaps also the failure of Chinese Ethnic policy. It is the Muslims who have accepted to transform their practice of Islam and adapt their way of life that have been relatively free by finding "how to adapt and reform in order to survive inside China while remaining active participants in the world of Islam" \autocite[19]{cherif-chebbiAbdAlWahhabLiu2016}.

\paragraph{}

The rest, those who preach and practice Islam according to conceptions of Muslim faith not shaped by the CCP, they lead a more restricted life. The CCP has imposed a National conception of faith, accepted only insofar as any foreign influence is tightly regulated by the Party. This is experienced both through an extraterritorial oppression of the Uyghur diaspora as well as "Officializing" interactions with the Muslim Ummah abroad.

The PRC is leading a "Sinicization" of Islam, controlling the religion through official State organs. This takes the form of registration of Imams, their preach and mosques with institutions such as the Islamic Association of China.

Lastly and most extreme is the CCP's establishment of "reeducation camps". Through internment and "education" the PRC is fighting what it generalizes as "extremism" while it is actually a culturally further away practice of Islam with a distinct culture and an independence movement.

\paragraph{}

As it stands, "we should not conclude that Islam is actually the source of Uyghur confrontation with the [...] Chinese state" \autocite[339]{starrXinjiangChinaMuslim2004}, rather, the State is persecuting the Uyghur and their identity which "is a complex and a composite of various sub-identities, of which being Muslim is one" \autocite[541]{kuoRevisitingSalafijihadistThreat2012}. As such, Muslims are relatively free in the PRC, so long as they preach Islam as accepted by the CCP not asserting a distinct cultural identity. In a sense, \textcite[183]{huntingtonClashCivilizationsRemaking1996}'s "Islamic intolerance, and Sinic asser­tiveness" is reversed in China where, specifically in Xinjiang, Islamic assertiveness opposes Sinic intolerance.

%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

\newpage

% \emergencystretch=1em

\section*{References}
\printbibliography[heading=none]

\vspace*{\fill}
% print license
\doclicenseThis

%----------------------------------------------------------------------------------------

\end{document}
